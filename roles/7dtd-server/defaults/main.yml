---
# Main configuration of the dedicated server files
server_user: zed
server_group: zed
server_home: /home/zed
server_install_dir: "{{ server_home }}/7dtd-dedicated-server"
server_install_beta: ~
server_data_dir: "{{ server_home }}/7dtd-data"
server_savegame_dir: "{{ server_data_dir }}/Saves"
server_bin_dir: "{{ server_home }}/bin"

# SteamCMD configuration
steamcmd_app_update_flags: []

# Backup configuration
server_backup_src_list:
  - "{{ server_home }}/.local"
  - "{{ server_data_dir }}"
  - "{{ server_install_dir }}/output-log__*.log"
  - "{{ server_bin_dir }}"
  - "{{ server_home }}/Mail"
server_backup_to_dir: /mnt/backup/zed/7DaysToDieDataBackups
server_backup_volumes:
  default:
    src_list: "{{ server_backup_src_list }}"
    dest_dir: "{{ server_backup_to_dir }}"
    max_level: 4
    max_per_level: 6
    max_full: 4
    generations: 2
    schedule:
      minute: "*/15"
      hour: "0,1,8-23"
      day: "*"
  full:
    src_list: "{{ server_backup_src_list }}"
    dest_dir: "{{ server_backup_to_dir }}"
    max_level: 1
    max_per_level: 1
    max_full: 8
    generations: 2
    schedule:
      minute: "1"
      hour: "2"
      day: "*/2"

# Mods configuration
server_mods_install: yes
server_mods_xpath:
  - KrampusMod
  # - KrampusMod_Airdrops
  - KrampusMod_Backpack
  - KrampusMod_Bigger_Stacks
  - KrampusMod_Biome_Weather
  # - KrampusMod_Extra_Zombie_XP
  # - KrampusMod_Faster_Smelting
  - KrampusMod_Gamestages
  - KrampusMod_LeftHUD
  - KrampusMod_Log_Spikes
  - KrampusMod_Perks
  - KrampusMod_Spawning
  - KrampusMod_Steel_Bars
  - KrampusMod_UI_Toolbelt
  - KrampusMod_Useful_Power_Tools
  - KrampusMod_Vehicles
  - S420_SimpleUI-4DigitCraft
  - S420_SimpleUI-Compass
  - S420_SimpleUI-CraftingQueue
  - S420_SimpleUI-ForgeInput
  # - S420_SimpleUI-Toolbelt
server_mods_installable:
  allocs: server_fixes_v19_22_32.tar.gz
server_mods_install_raw_config: yes
server_mods_raw_config_files: []

# Configuration of game and world settings.
server_config_visibility: 0 # unlisted
server_config_name: Krampusnacht | PvE | 18.4 Stable | Mods
server_config_desc: >-
  KrazyKrampus' 7 Days to Die Server. Server is a PvE, custom random generated map w/ loot enhancements,
  spawning mods, stack mods, XUi changes + more. All respectful players are welcome to join.
server_config_password: ~
server_config_max_player_count: 8
server_config_reserved_slots: 0
server_config_admin_slots: 1
server_config_game_world: Wayland
server_config_game_world_size: 8192 # only used for RWG
server_config_gen_seed: Wayland # controls the generation seed (only used for RWG)
server_config_game_name: Krampus # names the game files (affects save game name)
server_config_game_difficulty: 2
server_config_game_mode: GameModeSurvival
server_config_zombies_move: 0 # default (walk during day)
server_config_zombies_move_night: 2 # default (run)
server_config_zombies_feral_move: 3 # sprint
server_config_zombies_bloodmoon_move: 3 # sprint
server_config_cheat_mode: no
server_config_enemy_spawning: yes
server_config_enemy_difficulty: 0 # not feral
server_config_blood_moon_enemy_count: 12
server_config_blood_moon_frequency: 7
server_config_blood_moon_range: 0
server_config_blood_moon_warning: 6 # hour # when BM warning is shown
server_config_max_spawned_zombies: 60
server_config_drop_on_death: 0 # nothing
server_config_drop_on_quit: 0 # nothing
server_config_airdrop_frequency: 72
server_config_airdrop_marker: yes
server_config_loot_abundance: 50
server_config_loot_respawn_days: 30
server_max_world_xfer_kibs: 1024
server_config_max_spawned_animals: 80
server_config_max_allowed_view_distance: 10
server_config_xp_multiplier: 100
server_config_landclaim_count: 4
server_config_landclaim_size: 80
server_config_landclaim_deadzone: 200
server_config_landclaim_expiry_time: 14
server_config_landclaim_decay_mode: 2
server_config_landclaim_online_durability_modifier: 4
server_config_landclaim_offline_durability_modifier: 0
server_config_block_damage_player: 100
server_config_block_damage_ai: 100
server_config_block_damage_aibm: 100
server_config_day_night_length: 60 # real time minutes per in game day
server_config_day_light_length: 18 # hours of sunlight
server_config_player_killing_mode: 0
server_config_persistent_profiles: no
server_config_player_safe_zone_level: 5
server_config_player_safe_zone_hours: 24
server_config_player_bedroll_deadzone_size: 30
server_config_player_bedroll_expiry_time: 45
server_config_party_shared_kill_range: 200 # in "meters"
server_config_admin_controlpanel_enabled: yes
server_config_admin_controlpanel_port: 8080
server_config_admin_controlpanel_password: krampus1
server_config_admin_telnet_enabled: yes
server_config_admin_telnet_port: 8081
server_config_admin_password: krampus1
server_config_eac: no

# SteamIDs of various admin permissions.
server_admin_config_admin_users:
  # KrazyKrampus
  - steamID: "76561198003021188"
    permission_level: 0
  # Amalee215
  - steamID: "76561198889951492"
    permission_level: 1
  # GreenMonkeymax
  - steamID: "76561198839118278"
    permission_level: 50
  # # WeAreLegion
  # - steamID: "76561197985266562"
  #   permission_level: 10

# SteamIDs of bad players who get their accounts banned from the server.
server_admin_config_banned_users:
  # Dazzletrap
  - steamID: "76561198325217275"
    reason: Griefing/stealing server owner supplies.
    unbandate: "2030-01-01 00:00:00"
